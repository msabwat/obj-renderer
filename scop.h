/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/15 10:53:57 by msabwat           #+#    #+#             */
/*   Updated: 2020/12/15 10:59:27 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCOP_H
# define SCOP_H

typedef struct s_object {
	float *vertices;
	unsigned int *indices;
	unsigned int n_vertices;
	unsigned int n_indices;
} t_object;

void load_obj(t_object *obj, char *filename);

float *ident_mat4_4(float *mat);
float *new_mat(int cols, int lines);

#endif
