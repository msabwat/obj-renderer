/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/15 11:53:04 by msabwat           #+#    #+#             */
/*   Updated: 2020/12/16 01:20:16 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// malloc/realloc
#include <stdlib.h>
// glProcAddress
#include <GL/glew.h>
// window and keybd events
#include <GLFW/glfw3.h>

#include "libft.h"
#include "scop.h"

float *ident_mat4_4(float *mat) {
	float mat4[4][4] = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	};
	float *tmp = mat;
	int k = 0;
	for (int j=0; j < 4; j++) {
		for (int i=0; i < 4; i++) {
			tmp[k++] = mat4[j][i];
		}
	}
	return mat;
}

float *new_mat(int cols, int lines) {
	float *mat = (float *)malloc(sizeof(float) * lines * cols);
	float *tmp = mat;
	int k = 0;
	for (int j=0; j < lines; j++) {
		for (int i=0; i < cols; i++) {
			tmp[k++] = 0;
		}
	}
	return mat;
}


GLFWwindow* scop_init(char *filename) {
	// Initialise GLFW
	glewExperimental = 1; // Needed for core profile
	if( !glfwInit() )
		ft_putstr("Failed to initialize GLFW\n");
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); 
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); 

    // Open a window and create its OpenGL context
	GLFWwindow* window = glfwCreateWindow( 1024, 768, ft_strcat(filename, " - obj-renderer") , NULL, NULL);
	if( window == NULL ){
		ft_putstr("Failed to open GLFW window.\n");				
		glfwTerminate();
	}
	glfwMakeContextCurrent(window); // Initialize GLEW
	glewExperimental=1; // Needed in core profile
	if (glewInit() != GLEW_OK) {
		ft_putstr("Failed to initialize GLEW\n");
		glfwDestroyWindow(window);
		glfwTerminate();
	}
	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	return window;
}

GLuint load_program() {
	GLuint program;
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	const char *vertex_source =
		"#version 330 core\n"
		"layout(location = 0) in vec3 vertexPosition_modelspace;\n"
		"void main(){\n"
		"gl_Position.xyz = vertexPosition_modelspace;\n"
		"gl_Position.w = 1.0;\n"
		"}\n";
	const char *fragment_source =
		"#version 330 core\n"
		"out vec3 color;\n"
		"void main(){\n"
		"color = vec3(1,0,0);\n"
		"}\n";

	GLint Result = GL_FALSE;

	glShaderSource(VertexShaderID, 1, &vertex_source , NULL);
	glCompileShader(VertexShaderID);

	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	// glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( Result == GL_FALSE )
        ft_putstr("could not compile vtx shader\n");

	glShaderSource(FragmentShaderID, 1, &fragment_source , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	// glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( Result == GL_FALSE )
        ft_putstr("could not compile frgmt shader\n");

	program = glCreateProgram();
	glAttachShader(program, VertexShaderID);
	glAttachShader(program, FragmentShaderID);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &Result);
	// glGetProgramiv(program, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( Result == GL_FALSE )
		// const char* ProgramErrorMessage(InfoLogLength+1);
		// glGetProgramInfoLog(program, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        ft_putstr("could not link program\n");

	glDetachShader(program, VertexShaderID);
	glDetachShader(program, FragmentShaderID);
	
	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);
	
	return program;
}

int main(int ac, char **av) {
	if ((ac > 2) || (ac == 1))
		return 1;
	t_object *obj = malloc(sizeof(t_object));

	if (!obj)
		return -1;
	
	obj->vertices = NULL;
	obj->indices = NULL;
	obj->n_vertices = 0;
	obj->n_indices = 0;
	
	load_obj(obj, av[1]);
	GLFWwindow* window = scop_init(av[1]);
	GLuint program = load_program();
	
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, obj->n_vertices * sizeof(float),
			&obj->vertices[0], GL_STATIC_DRAW);

	GLuint ibo;
	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, obj->n_indices * sizeof(unsigned int),
				 &obj->indices[0], GL_STATIC_DRAW);

	do{
		glClear( GL_COLOR_BUFFER_BIT );
		glUseProgram(program);
	
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

        // Draw the triangle !
		//glDrawArrays(GL_TRIANGLES, 0, obj->n_vertices/3);
		glDrawElements(GL_TRIANGLES, obj->n_indices, GL_UNSIGNED_INT, (void*)0);
		glDisableVertexAttribArray(0);
		
		glfwSwapBuffers(window);
		glfwPollEvents();
		
	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );
	
	free(obj->vertices);
	free(obj->indices);
	free(obj);

	glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
	
	glfwDestroyWindow(window);
	glfwTerminate();
	
	return 0;
}
