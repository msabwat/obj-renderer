/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/15 10:55:22 by msabwat           #+#    #+#             */
/*   Updated: 2020/12/16 01:17:23 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "scop.h"

static int add_vertex(char *str, unsigned int *index, float **vertices)
{
	float *tab;
	int i = 0;
	if (*vertices) {
		*vertices = (float *)realloc(*vertices, sizeof(float) * (*index+3));
		if (!*vertices)
			return -1;
		tab = *vertices;
		tab += *index;
	}
	else {
		*vertices = malloc(sizeof(float) * 3);
		if (!*vertices)
			return -1;
		tab = *vertices;
	}
	if (str) {
		char **list = ft_strsplit(str, ' ');
		char **tmp = list;
		while ((*tmp != NULL) && (i < 3)) {
			*tab = atof(*tmp);
			tab++;
			*index = *index + 1;
			i++;
			(void)*tmp++;
		}
		tmp=list;
		while (*tmp != NULL) {
			free(*tmp);
			(void)*tmp++;
		}
		free(list);
		if (i < 3) {
			return -1;
		}
	}
	// tout va bien
	return i;
}

static int get_cmd_param(char **tmp, unsigned int index)
{
	int result;
	char **cmds = ft_strsplit(tmp[index], '/');
	char **ptr = cmds;
	if (cmds[0] != 0) {
		result = ft_atoi(cmds[0]);
	}
	else
		result = ft_atoi(tmp[index]);

	while (*cmds != NULL) {
		free(*cmds);
		(void)*cmds++;
	}
	free(ptr);
	return result;
}

static int add_face(char *str, unsigned int *index, unsigned int **indices)
{
	unsigned int *tab;
	int indexes = 0;
	char **tmp;
	char **list = NULL;
	
	if (str) {
		list = ft_strsplit(str, ' ');
		tmp = list;
		while (*tmp != NULL) {
			(void)*tmp++;
			indexes += 1;
		}
	}
	tmp=list;
	int triangle_num = indexes/3;
	int ind = indexes;
	if ((ind % 3 == 1) || (ind % 3 == 2)) {
		triangle_num += 1;
	}
	if (triangle_num >= 1) {
		if (*indices) {
			*indices = (unsigned int *)realloc(*indices, sizeof(unsigned int) * (*index + indexes));
			if (!*indices)
				return -1;
			tab = *indices;
 			tab += *index;
		}
		else {
			*indices = malloc(sizeof(unsigned int) * indexes);
			if (!*indices)
				return -1;
			tab = *indices;
		}
		while (triangle_num != 0) {
			while ((ind > 5) || (ind == 3)) {
				for (int i=0; i<3; i++) {					
					*tab = get_cmd_param(tmp, i);
					*index += 1;
					tab++;
				}
				triangle_num = triangle_num - 1;
				ind = ind - 3;
			}
			if (ind % 3 == 1 && ind < 5) {
				for (int i=0; i<3; i++) {
					*tab = get_cmd_param(tmp, i);
					tab++;
				}
				*indices = (unsigned int *)realloc(*indices, sizeof(unsigned int) * (*index+indexes+ 3));
				if (!*indices)
					return -1;
				tab = *indices;
				tab += *index + 3;
				*tab = get_cmd_param(tmp, 3);
				tab++;
				*tab = get_cmd_param(tmp, 0);
				tab++;
				*tab = get_cmd_param(tmp, 2);
				triangle_num = triangle_num - 2;
				ind = ind - 6;
				*index = *index + indexes + 2;	
			}
			else if (ind % 3 == 2 && ind < 6) {
				for (int i=0; i<3; i++){
					*tab = get_cmd_param(tmp, i);
					tab++;
				}
				*indices = (unsigned int *)realloc(*indices, sizeof(unsigned int) * (*index+indexes+ 2));
				if (!*indices)
					return -1;
				tab = *indices;
				tab += *index + 3;
				*tab = get_cmd_param(tmp, 3);
				tab++;
				*tab = get_cmd_param(tmp, 4);
				tab++;
				*tab = get_cmd_param(tmp, 0);
				tab++;
				triangle_num = triangle_num - 2;
				ind = ind - 6;
				*index = *index + indexes + 1;	
			}
		}
	}
	tmp=list;
	while (*tmp != NULL) {
		free(*tmp);
		(void)*tmp++;
	}
	free(list);
	if (indexes < 3)
		return -1;
	return indexes;
}

void load_obj(t_object *obj, char *filename) {
	int fd = open(filename, O_RDWR);
	char *str = NULL;
	char *cmd = str;
	int res;

	ft_putstr("loading ... ");
	ft_putstr(filename);
	ft_putstr("\n");
	
	while (42) {
		res = get_next_line(fd, &str);
		if (res == 0 || res == -1)
			break;
		cmd = str;
		if (cmd == NULL)
			abort();
		if (*cmd == 'v') {
			cmd++;
			res = add_vertex(cmd, &obj->n_vertices, &(obj->vertices));
			if (res == -1)
				break;	
		}
		else if (*cmd == 'f') {
			cmd++;
			res = add_face(cmd, &obj->n_indices, &(obj->indices));
			if (res == -1)
				break;
		}
		free(str);
	}
	ft_putstr("finished loading object\n");
	ft_putstr("loaded ");
	ft_putnbr(obj->n_vertices);
	ft_putstr(" coordinates ");
	ft_putnbr(obj->n_vertices/3);
	ft_putstr(" vertex/vertices\n");
	ft_putstr("found ");
	ft_putnbr(obj->n_indices);
	ft_putstr(" indices ");
	ft_putnbr(obj->n_indices/3);
	ft_putstr(" triangle(s)\n");
}
