#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/12/03 00:06:30 by msabwat           #+#    #+#              #
#    Updated: 2020/12/16 02:48:56 by msabwat          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME=scop

FLAGS=-Wall -Wextra -Werror

CC=clang

INC=-I.

LIBFT_INC=-Ilibft/

SRC_NAME=load.c

OBJ_NAME=$(SRC_NAME:.c=.o)

SRC_PATH=src

OBJ_PATH=.obj

SRC=$(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJS=$(addprefix $(OBJ_PATH)/,$(OBJ_NAME)) 

all: makedir $(NAME)

makedir:
	@mkdir -p .obj

test: makedir $(OBJS)
	$(MAKE) -C libft
	$(CC) $(FLAGS) -g -fsanitize=address main.c $(OBJS) $(LIBFT_INC) $(INC) -lGLEW -lglfw -lGL -L libft/ -lft -o $(NAME)

$(NAME): main.o $(OBJS)
	$(MAKE) -C libft
	$(CC) $(FLAGS) main.o $(OBJS) $(LIBFT_INC) $(INC) -lGLEW -lglfw -lGL -L libft/ -lft -o $(NAME)

main.o: main.c
	 $(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c main.c
$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c $< -o $@

clean:
	rm -fr $(NAME)

fclean: clean
	rm -fr $(OBJ_PATH)
	rm -fr main.o
	$(MAKE) fclean -C libft

re: fclean all
